/**
 * Registramos un store ContactoStore
 */
Ext.regStore('ContactoStore', {
    /**
     * Indicamos instancias de que modelo tendra
     */
    model: 'ContactoModel',
    /**
     * indicamos por que campo se ordenara, cada objeto es un orden y su sentido, y se pueden
     * establecer mas de un campo de orden
     */
    sorters: [{
        property: 'nombre',
        direction: 'DESC'
    }],
    /**
     * Aqui por lo general tenemos las operaciones CRUD (create,read,update,delete), indicamos el origen de donde tendremos
     * los datos (url), el tipo (local o remoto) de origen, definimos las operaciones para las 4 operaciones CRUD,
     * definimos un lector que sera el que defina como se buscara y cargara los datos
     */
    proxy: {
        type: 'localstorage', // el tipo puede ser ajax, scripttag, localstorage, sessionstorage, memory
        id: 'contactos_app' // id que unico que contendra toda la data que almacenemos en el store
    },
    /**
     * Indicamos que los datos del modelo se agrupe por la primera letra del campo nombre
     */
    getGroupString: function (record) {
        if (record && record.data.nombre) {
            return record.get('nombre')[0];
        } else {
            return '';
        }
    }
});
/**
 * Almacenamos el store previamente creado al namespace stores de nuestra aplicacion
 */
ContactoApp.stores.contactoStore = Ext.StoreMgr.get('ContactoStore');
